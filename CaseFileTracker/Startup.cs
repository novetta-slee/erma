﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CaseFileTracker.Startup))]
namespace CaseFileTracker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
