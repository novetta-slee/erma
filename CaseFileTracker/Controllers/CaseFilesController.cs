﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace CaseFileTracker.Controllers
{
    [Authorize]
    public class CaseFilesController : Controller
    {
        private FileTrackerEntities db = new FileTrackerEntities();

        #region Views

        // GET: CaseFiles
        public ActionResult Index()
        {
            return View(db.CaseFiles.OrderBy(c => c.LastName).ToList());
        }

        // GET: CaseFiles
        [ActionName("IndexFiltered")]
        public ActionResult Index(string term)
        {
            List<CaseFile> caseFiles = db
                .CaseFiles
                .Where(c => c.LastName.Contains(term) || c.FirstName.Contains(term))
                .OrderBy(c => c.LastName)
                .ToList();

            return View("Index", caseFiles);
        }

        // GET: CaseFiles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CaseFile caseFile = db.CaseFiles.Find(id);
            if (caseFile == null)
            {
                return HttpNotFound();
            }
            return View(caseFile);
        }

        // GET: CaseFiles/Create
        public ActionResult Create()
        {
            return View();
        }

        #endregion Views

        #region CRUD

        // POST: CaseFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,LastName,FirstName,Middle,DOB,SSN")] CaseFile caseFile)
        {
            if (ModelState.IsValid)
            {
                db.CaseFiles.Add(caseFile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(caseFile);
        }

        // GET: CaseFiles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CaseFile caseFile = db.CaseFiles.Find(id);
            if (caseFile == null)
            {
                return HttpNotFound();
            }
            return View(caseFile);
        }

        // POST: CaseFiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LastName,FirstName,Middle,DOB,SSN")] CaseFile caseFile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(caseFile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(caseFile);
        }

        // GET: CaseFiles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CaseFile caseFile = db.CaseFiles.Find(id);
            if (caseFile == null)
            {
                return HttpNotFound();
            }
            return View(caseFile);
        }

        // POST: CaseFiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CaseFile caseFile = db.CaseFiles.Find(id);
            db.CaseFiles.Remove(caseFile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion CRUS

        #region Misc

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult Search(string term)
        {
            CaseFile[] results = db
                .CaseFiles
                .Where(c => c.LastName.Contains(term) || c.FirstName.Contains(term))
                .OrderBy(c => c.LastName)
                .ToArray();

            return Json(results, JsonRequestBehavior.AllowGet);

        }

        #endregion Misc
    }
}
