﻿using System.Web.Mvc;

namespace CaseFileTracker.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated) Response.Redirect("/Account/Login");

            return View();
        }
    }
}