﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace CaseFileTracker.Controllers
{
    public class RequestsController : Controller
    {
        private FileTrackerEntities db = new FileTrackerEntities();

        #region Views

        [AllowAnonymous]
        public ActionResult GetCaseFile()
        {
            return View();
        }

        // GET: Requests
        public ActionResult Index()
        {
            var requests = db.Requests.Include(r => r.AspNetUser).Include(r => r.CaseFile).Include(r => r.RequestType);
            return View(requests.ToList());
        }

        // GET: Requests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Request request = db.Requests.Find(id);
            if (request == null)
            {
                return HttpNotFound();
            }
            return View(request);
        }

        // GET: Requests/Create
        public ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email");
            //ViewBag.CaseFileID = new SelectList(db.CaseFiles, "ID", "LastName");
            ViewBag.RequestTypeID = new SelectList(db.RequestTypes, "ID", "Description");
            return View();
        }

        // POST: Requests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CaseFileID,UserID,RequestedBy,RequestTypeID,DateRequested,Location,DateReceived,DateReturned,Notes")] Request request)
        {
            // Initialize date requested
            request.DateRequested = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Requests.Add(request);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", request.UserID);
            //ViewBag.CaseFileID = new SelectList(db.CaseFiles, "ID", "LastName", request.CaseFileID);
            ViewBag.RequestTypeID = new SelectList(db.RequestTypes, "ID", "Description", request.RequestTypeID);

            return View(request);
        }

        // GET: Requests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Request request = db.Requests.Find(id);
            if (request == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", request.UserID);
            ViewBag.CaseFileID = new SelectList(db.CaseFiles, "ID", "LastName", request.CaseFileID);
            ViewBag.RequestTypeID = new SelectList(db.RequestTypes, "ID", "Description", request.RequestTypeID);
            return View(request);
        }

        // POST: Requests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CaseFileID,UserID,RequestedBy,RequestTypeID,DateRequested,Location,DateReceived,DateReturned,Notes")] Request request)
        {
            if (ModelState.IsValid)
            {
                db.Entry(request).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", request.UserID);
            ViewBag.CaseFileID = new SelectList(db.CaseFiles, "ID", "LastName", request.CaseFileID);
            ViewBag.RequestTypeID = new SelectList(db.RequestTypes, "ID", "Description", request.RequestTypeID);
            return View(request);
        }

        // GET: Requests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Request request = db.Requests.Find(id);
            if (request == null)
            {
                return HttpNotFound();
            }
            return View(request);
        }

        // POST: Requests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Request request = db.Requests.Find(id);
            db.Requests.Remove(request);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion Views

        #region Misc

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult SearchByName(string term)
        {
            CaseFile[] subjects = db
                .CaseFiles
                .Where(r => r.LastName.Contains(term) || r.FirstName.Contains(term))
                .OrderBy(r => r.LastName)
                .ToArray();

            return Json(
                subjects.Select(r => 
                    new { id = r.ID, value = string.Format("{0}, {1}", r.LastName, r.FirstName) }
                ), 
                JsonRequestBehavior.AllowGet
            );
        }

        #endregion Misc
    }
}