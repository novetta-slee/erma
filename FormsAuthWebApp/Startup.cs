﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FormsAuthWebApp.Startup))]
namespace FormsAuthWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
