﻿using System;
using System.Web.Services;

namespace S2MSServices
{
    /// <summary>
    /// Summary description for s2msauth
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class S2MSAuth : System.Web.Services.WebService
    {

        [WebMethod]
        public string Authenticate(string xml)
        {
            //return "<response><validUser>0</validUser><description>Your S2MS account has been locked</description></response>";
            return "<response><validUser>1</validUser><description></description></response>";
        }
    }

}