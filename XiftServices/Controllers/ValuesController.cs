﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace XiftServices.Controllers
{
    public class ValuesController : ApiController
    {
        public IHttpActionResult GetCaseFile(string uniqueID, string dob, string ssn)
        {
            dynamic caseFile;

            if (uniqueID == "0000-0000-0000-0000")
            {
                caseFile = new
                {
                    RecordFound = false
                };
            }
            else
            {
                caseFile = new
                {
                    RecordFound = true,
                    ID = 1,
                    Name = "Tanaka, Masahiro",
                    SSN = "000112222",
                    DoB = "11/19/1984"
                };
            }

            return Ok(caseFile);
        }

    }
}