//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ERMA
{
    using System;
    using System.Collections.Generic;
    
    public partial class STATUS
    {
        public STATUS()
        {
            this.FILEREQUESTs = new HashSet<FILEREQUEST>();
            this.FILEREQUESTAUDITs = new HashSet<FILEREQUESTAUDIT>();
        }
    
        public int ID { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
    
        public virtual ICollection<FILEREQUEST> FILEREQUESTs { get; set; }
        public virtual ICollection<FILEREQUESTAUDIT> FILEREQUESTAUDITs { get; set; }
    }
}
