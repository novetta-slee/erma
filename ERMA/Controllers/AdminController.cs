﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;

namespace ERMA.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {

        #region Locals

        ERMAEntities db = new ERMAEntities();

        #endregion Locals

        #region Routes

        [AllowAnonymous]
        public ActionResult Login()
        {
            string userName = Request.LogonUserIdentity.Name;
            string xml = string.Format("<request><userName>{0}</userName></request>", userName);
            string xmlResponse = string.Empty;

            // Test if we want to authenticate via S2MS
            if (bool.Parse(ConfigurationManager.AppSettings["s2msAuthorization"].ToString()))
            {
                // Put endpoint in web.config
                S2MSWCF.S2MSServiceClient auth = new S2MSWCF.S2MSServiceClient();
                xmlResponse = auth.Authenticate(xml);
            }
            else
            {
                // Let anyone in (until S2MS provides the web service)
                xmlResponse = "<response><validUser>1</validUser><description></description></response>";
            }

            XmlDocument xd = new XmlDocument();
            xd.LoadXml(xmlResponse);

            XmlNode validUser = xd.GetElementsByTagName("validUser")[0];

            // Test response
            if (validUser.FirstChild.Value == "1")
            {
                string url = (Request["ReturnUrl"] == null || Request["ReturnUrl"] == "/Home/LogOut") ? 
                    "/Home/Index" : Request["ReturnUrl"];
                FormsAuthentication.SetAuthCookie(userName, false);
                return RedirectToLocal(url);
            }
            else
            {
                // Set error message to inform user
                ViewBag.ErrorMessage = xd.GetElementsByTagName("description")[0].FirstChild.Value;
                return View();
            }

        }

        public ActionResult RoleManager()
        {
            List<USERROLE> userRoles = new List<USERROLE>();
            ViewData["Roles"] = db.ROLEs.OrderBy(r => r.NAME).ToDictionary(r => r.ID, r => r.NAME);

            // Test for name filter
            if (Request["nm"] != null)
            {
                string searchText = Request["nm"].ToString();
                userRoles = db
                    .USERROLEs
                    .Where(u => u.USERNAME.ToUpper().Contains(searchText.ToUpper()))
                    .OrderBy(u => u.USERNAME)
                    .ToList();
            }

            // Test for role filter
            if (Request["rid"] != null && Request["rid"] != "0" && Request["rid"] != string.Empty)
            {
                int roleID = int.Parse(Request["rid"].ToString());
                // Check for name filtered results
                if (userRoles.Count == 0)
                    userRoles = db.USERROLEs.Where(u => u.ROLEID == roleID).OrderBy(u => u.USERNAME).ToList();
                else
                    userRoles = userRoles.Where(u => u.ROLEID == roleID).OrderBy(u => u.USERNAME).ToList();
            }

            ViewData["RoleList"] = db.ROLEs.OrderBy(r => r.NAME).ToList();

            return View(userRoles);
        }

        #endregion Routes

        #region JSon

        public JsonResult CreateUserRole(int roleID, string userName)
        {
            try
            {
                // Create user role
                USERROLE userRole = new USERROLE();
                userRole.ROLEID = roleID;
                userRole.USERNAME = userName;
                db.USERROLEs.Add(userRole);
                db.SaveChanges();
                // Return successful response
                return Json(new { Success = true, Message = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Return failure
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteUserRole(int id)
        {
            USERROLE userRole = db.USERROLEs.Where(u => u.ID == id).FirstOrDefault();

            // Ensure user role exists
            if (userRole == null)
            {
                // Return failure
                return Json(new { Success = false, Message = "User role not found" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                // Delete role
                db.USERROLEs.Remove(userRole);
                db.SaveChanges();
                // Return successful response
                return Json(new { Success = true, Message = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Return failure
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateUserRole(int id, int roleID, string userName)
        {
            USERROLE userRole = db.USERROLEs.Where(u => u.ID == id).FirstOrDefault();

            // Ensure user role exists
            if (userRole == null)
            {
                // Return failure
                return Json(new { Success = false, Message = "User role not found" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                // Update user role
                userRole.ROLEID = roleID;
                userRole.USERNAME = userName;
                db.SaveChanges();
                // Return successful response
                return Json(new { Success = true, Message = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Return failure
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion JSon

        #region Private Methods

        /// <summary>
        /// Redirects to the original ERMA url the current user hit before 
        /// being redirected to login.
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        #endregion Private Methods

    }

}