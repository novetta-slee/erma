﻿using ERMA.Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;
using System.Web.Security;

namespace ERMA.Controllers
{
    public class HomeController : Controller
    {

        #region Locals

        ERMAEntities db = new ERMAEntities();

        #endregion Locals

        #region Routes

        [AllowAnonymous]
        public ActionResult AccessDenied()
        {
            return View();
        }

        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        public ActionResult AllRequests()
        {
            List<RequestResult> requests = GetRequestsByStatus(false);

            ViewBag.StatusCounts = GetStatusCounts(false);

            return View(requests);
        }

        [ERMAAuthorize]
        public ActionResult CoverSheet()
        {
            int id = int.Parse(Request["id"].ToString());

            CASEFILE caseFile = db.CASEFILEs.Where(c => c.ID == id).FirstOrDefault();

            return View(caseFile);
        }

        [AllowAnonymous]
        public ActionResult Error()
        {
            ViewBag.ErrorMessage = Session["LastError"].ToString();

            return View();
        }

        [ERMAAuthorize]
        public ActionResult Index()
        {
            bool canSeeAll = Roles.IsUserInRole(User.Identity.Name, "Administrator") || Roles.IsUserInRole(User.Identity.Name, "FileRoom");

            ViewBag.StatusCounts = GetStatusCounts(!canSeeAll);

            return View();
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            Response.StatusCode = 401;
            Response.StatusDescription = "Unauthorized";
            //Response.End();

            return View();
        }

        [ERMAAuthorize]
        public ActionResult MyRequests()
        {
            List<RequestResult> requests = GetRequestsByStatus(true);

            ViewBag.StatusCounts = GetStatusCounts(true);

            return View(requests);
        }

        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        public ActionResult Print()
        {
            int statusID = Request["status"] == null ? 1 : int.Parse(Request["status"].ToString());
            string sortCol = Request["sortCol"] == null ? "ActionDate" : Request["sortCol"].ToString();
            string sortDir = Request["sortDir"] == null ? "DESC" : Request["sortDir"].ToString();

            List<ListRequest> requests = db
                .FILEREQUESTAUDITs
                .Where(a => a.FILEREQUEST.STATUSID == statusID && a.CASEFILE.ACTIVE && a.FILEREQUEST.ACTIVE)
                .Select(a => new ListRequest
                {
                    DOB = a.CASEFILE.DOB,
                    FullName = a.CASEFILE.FULLNAME,
                    RequestedBy = a.REQUESTEDBY,
                    ActionDate = a.ACTIONDATE,
                    SSN = a.CASEFILE.SSN
                })
                .OrderBy(string.Format("{0} {1}", sortCol, sortDir))
                .ToList<ListRequest>();

            return View(requests);
        }

        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        [HttpGet]
        public ActionResult SearchForCaseFile(string searchText)
        {
            string[] searchTerms = searchText.Split(' ');
            System.Text.StringBuilder whereClause = new System.Text.StringBuilder();

            foreach (string searchTerm in searchTerms)
            {
                whereClause.AppendFormat("FULLNAME.ToUpper().Contains(\"{0}\") && ", searchTerm.ToUpper());
            }
            whereClause.Append("ACTIVE");

            List<CASEFILE> caseFiles = db
                .CASEFILEs
                .Where(whereClause.ToString())
                .OrderBy(c => c.LASTNAME)
                .ToList();

            return PartialView("_CaseFileSearch", caseFiles);
        }

        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        public ActionResult SearchForFile()
        {
            ViewBag["DoB"] = TempData["dob"];
            ViewBag["FullName"] = TempData["fullName"];
            ViewBag["ID"] = TempData["id"];
            ViewBag["SSN"] = TempData["ssn"];

            List<CASEFILE> caseFiles = new List<CASEFILE>();

            if (Request["nm"] != null)
            {
                string searchText = Request["nm"].ToString();
                caseFiles = db
                    .CASEFILEs
                    .Where(c => c.FULLNAME.ToUpper().Contains(searchText.ToUpper()) && c.ACTIVE)
                    .OrderBy(c => c.FULLNAME)
                    .ToList();
            }

            return View(caseFiles);
        }

        [ERMAAuthorize]
        [ValidateInput(false)]
        public ActionResult S2MS(DateTime dob, string fullName, int id, string ssn)
        {
            // Retrieve current case file
            CASEFILE caseFile = db.CASEFILEs.Where(c => c.PERSONID == id).FirstOrDefault();

            // Test for case file
            if (caseFile != null)
            {
                // Show the user the request status
                ViewData["ID"] = caseFile.ID;
                ViewData["PersonID"] = caseFile.PERSONID;
                ViewData["RequestID"] = 0;
                ViewData["FullName"] = caseFile.FULLNAME.Trim();
                ViewData["DOB"] = caseFile.DOB.ToShortDateString();
                ViewData["SSN"] = caseFile.SSN;
                ViewData["RequestDate"] = string.Empty;
                ViewData["STATUSID"] = 0;
                ViewData["Status"] = string.Empty;
                // Determine if case file is digitized
                if (caseFile.ISDIGITIZED)
                {
                    // Set status so front end shows files
                    ViewData["Status"] = "HasFiles";
                }
                else
                {
                    // Determine if current user has an outstanding, active request for this file
                    FILEREQUEST request = db
                        .FILEREQUESTs
                        .Where(r => r.CASEFILE.PERSONID == id && r.REQUESTEDBY == User.Identity.Name && r.STATUSID < 3 && r.ACTIVE)
                        .FirstOrDefault();
                    // Test request
                    if (request != null)
                    {
                        // Show the user the request status
                        ViewData["RequestID"] = request.ID;
                        ViewData["RequestDate"] = request.REQUESTDATE;
                        ViewData["StatusID"] = request.STATUSID;
                        ViewData["Status"] = request.STATUS.NAME;
                        // Test if current status is issued
                        if (request.STATUSID == 2)
                        {
                            // Retrieve current audit
                            FILEREQUESTAUDIT audit = db.FILEREQUESTAUDITs.Where(a => a.FILEREQUESTID == request.ID && a.STATUSID == 2).FirstOrDefault();
                            if (audit != null)
                            {
                                ViewData["AuditID"] = audit.ID;
                                ViewData["IssueDate"] = audit.ACTIONDATE;
                            }
                        }
                    }
                }
            }
            else
            {
                ViewData["DOB"] = dob.ToShortDateString();
                ViewData["SSN"] = ssn;
                ViewData["FullName"] = fullName;
                ViewData["PersonID"] = id;
                ViewData["Status"] = "NotFound";
                ViewData["StatusID"] = 0;
            }

            return View(caseFile);
        }

        [ERMAAuthorize(Roles = "Administrator,Scanner")]
        public ActionResult Scan()
        {
            List<RequestResult> requests = GetRequestsByStatus(false);

            return View(requests);
        }

        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        public ActionResult Subjects()
        {
            List<CASEFILE> caseFiles = new List<CASEFILE>();

            if (Request["nm"] != null && Request["nm"] != string.Empty)
            {
                int page = Request["page"] == null ? 1 : int.Parse(Request["page"].ToString());
                int pageSize = int.Parse(ConfigurationManager.AppSettings["pageSize"].ToString());
                int skip = (page - 1) * pageSize;
                string searchText = Request["nm"].ToString();

                string[] searchTerms = searchText.Split(' ');
                System.Text.StringBuilder whereClause = new System.Text.StringBuilder();

                foreach (string searchTerm in searchTerms)
                {
                    whereClause.AppendFormat("FULLNAME.ToUpper().Contains(\"{0}\") && ", searchTerm.ToUpper());
                }
                whereClause.Append("ACTIVE");

                //int total = db.CASEFILEs.Where(c => c.FULLNAME.ToUpper().Contains(searchText.ToUpper()) && c.ACTIVE).Count();
                int total = db.CASEFILEs.Where(whereClause.ToString()).Count();

                int remainder;
                int pages = Math.DivRem(total, pageSize, out remainder);

                if (remainder > 0) pages++;
                ViewBag.PageCount = pages;
                ViewBag.CurrentPage = page;

                caseFiles = db
                    .CASEFILEs
                    .Where(whereClause.ToString())
                    .OrderBy(c => c.LASTNAME)
                    .Skip(skip)
                    .Take(pageSize)
                    .ToList();
            }

            return View(caseFiles);
        }

        [HttpPost]
        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        public JsonResult UploadCaseFile()
        {
            int id = int.Parse(Request["uploadCaseFileID"].ToString());
            string name = Request["casefileDocName"].ToString();
            string description = Request["casefileDocDescription"].ToString();

            CASEFILE caseFile = db.CASEFILEs.Where(c => c.ID == id).FirstOrDefault();
            CASEFILEDOC cfd = new CASEFILEDOC();

            bool hasError = false;
            string actionID = string.Empty;
            string directory = string.Empty;
            string exception = string.Empty;
            string message = string.Empty;
            string pdfPath = string.Empty;

            // Ensure a file was posted
            if (Request.Files.Count > 0)
            {
                var digitalCaseFile = Request.Files[0];

                if (caseFile != null)
                {
                    if (digitalCaseFile.ContentLength > 0)
                    {
                        string fileName = string.Empty;
                        try
                        {
                            // Test for existing action id
                            if (caseFile.XIFTACTION == null)
                                // Retrieve action/unique id
                                actionID = GetNextXiftID(caseFile.CASEFILEDATE.Year);
                            else
                                actionID = caseFile.XIFTACTION;
                            // Get this from CMS table (or CASEFILE if it's a direct import from CMS)
                            fileName = Path.GetRandomFileName().Split('.')[0] + ".pdf";
                            // Retrieve file repository path
                            directory = ConfigurationManager.AppSettings["fileRepo"].ToString();

                            // Test for existence of directory
                            if (!Directory.Exists(directory))
                            {
                                // Create directory
                                Directory.CreateDirectory(directory);
                            }

                            // Build path
                            pdfPath = Path.Combine(new string[] { directory, fileName });
                            // Save file to file system
                            digitalCaseFile.SaveAs(pdfPath);
                            // Update case file
                            caseFile.ISDIGITIZED = true;
                            caseFile.XIFTACTION = actionID;
                            db.SaveChanges();
                            // Send email to notify requesters it has been digitized
                            //ERMAEmail.CaseFileDigitized(id, User.Identity.Name);
                        }
                        catch (Exception ex)
                        {
                            hasError = true;
                            message = "An error occurred uploading the file";
                            exception = ex.ToString();
                        }
                        try
                        {
                            // Create document object
                            cfd.ACTIVE = true;
                            cfd.CASEFILEID = caseFile.ID;
                            cfd.DESCRIPTION = description;
                            cfd.DOCUMENT = BlobFile(pdfPath);
                            cfd.FILENAME = fileName;
                            cfd.NAME = name;
                            cfd.UPLOADDATE = DateTime.Now;
                            cfd.UPLOADEDBY = User.Identity.Name;
                            db.CASEFILEDOCs.Add(cfd);
                            db.SaveChanges();
                            // Update IsDigitized based on column count
                            UpdateIsDigitized(cfd.CASEFILE.ID, cfd.CASEFILE.ISDIGITIZED);
                        }
                        catch (Exception ex)
                        {
                            hasError = true;
                            message = "An error occurred updating the database";
                            exception = ex.ToString();
                        }
                    }
                    else
                    {
                        // Return error regarding file without content
                        hasError = true;
                        message = "File contains no content";
                    }
                }
                else
                {
                    // Return error regarding null file
                    hasError = true;
                    message = "No file found";
                }

                // Clean files in upload directory
                DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["fileRepo"].ToString());
                FileInfo[] tempFiles = di.GetFiles().ToArray();
                try
                {
                    foreach (FileInfo tempFile in tempFiles) tempFile.Delete();
                }
                catch
                {
                    // Return message, but don't show error to user
                    message = "Error cleaning uploaded files.";
                }
            }

            return Json(
                new
                {
                    HasError = hasError,
                    Message = message,
                    Exception = exception,
                    Document = new
                    {
                        ID = cfd.ID,
                        FILENAME = cfd.FILENAME,
                        FILEPATH = "Deprecated",
                        NAME = cfd.NAME,
                        DESCRIPTION = cfd.DESCRIPTION,
                        UPLOADDATE = cfd.UPLOADDATE.ToString()
                    }
                }
            );
        }

        [HttpPost]
        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        public JsonResult UploadCaseFile_OLD()
        {
            int id = int.Parse(Request["uploadCaseFileID"].ToString());
            string name = Request["casefileDocName"].ToString();
            string description = Request["casefileDocDescription"].ToString();

            CASEFILE caseFile = db.CASEFILEs.Where(c => c.ID == id).FirstOrDefault();
            CASEFILEDOC cfd = new CASEFILEDOC();

            bool hasError = false;
            string actionID = string.Empty;
            string directory = string.Empty;
            string exception = string.Empty;
            string message = string.Empty;

            if (Request.Files.Count > 0)
            {
                var digitalCaseFile = Request.Files[0];

                if (caseFile != null)
                {
                    if (digitalCaseFile.ContentLength > 0)
                    {
                        string fileName = string.Empty;
                        try
                        {
                            // Test for existing action id
                            if (caseFile.XIFTACTION == null)
                                // Retrieve action/unique id
                                actionID = GetNextXiftID(caseFile.CASEFILEDATE.Year);
                            else
                                actionID = caseFile.XIFTACTION;
                            // Get this from CMS table (or CASEFILE if it's a direct import from CMS)
                            fileName = actionID + ".pdf";
                            // Retrieve file repository path
                            directory = ConfigurationManager.AppSettings["fileRepo"].ToString();
                            directory = Path.Combine(new string[] { directory, caseFile.CASEFILEDATE.Year.ToString(), actionID });
                            // Test for existence of directory
                            if (!Directory.Exists(directory))
                            {
                                // Create directory
                                Directory.CreateDirectory(directory);
                            }
                            else
                            {
                                // Retrieve existing pdf files
                                string[] fileListing = Directory.GetFiles(directory, "*.pdf");
                                bool availableFileName = false;
                                int counter = 1;
                                // Search for an available file name
                                while (!availableFileName)
                                {
                                    if (fileListing.Contains(Path.Combine(directory, string.Format("{0}_{1}.pdf", actionID, counter))))
                                    {
                                        counter++;
                                    }
                                    else
                                    {
                                        fileName = string.Format("{0}_{1}.pdf", actionID, counter);
                                        availableFileName = true;
                                    }
                                }
                            }
                            // Build path
                            string pdfPath = Path.Combine(new string[] { directory, fileName });
                            // Save file to file system
                            digitalCaseFile.SaveAs(pdfPath);
                            // Update case file
                            caseFile.ISDIGITIZED = true;
                            caseFile.XIFTACTION = actionID;
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            hasError = true;
                            message = "An error occurred uploading the file";
                            exception = ex.ToString();
                        }
                        try
                        {
                            // Create document object
                            cfd.ACTIVE = true;
                            cfd.CASEFILEID = caseFile.ID;
                            cfd.DESCRIPTION = description;
                            cfd.FILENAME = fileName;
                            cfd.NAME = name;
                            cfd.UPLOADDATE = DateTime.Now;
                            cfd.UPLOADEDBY = User.Identity.Name;
                            db.CASEFILEDOCs.Add(cfd);
                            db.SaveChanges();
                            // Update IsDigitized based on column count
                            UpdateIsDigitized(cfd.CASEFILE.ID, cfd.CASEFILE.ISDIGITIZED);
                        }
                        catch (Exception ex)
                        {
                            hasError = true;
                            message = "An error occurred updating the database";
                            exception = ex.ToString();
                        }
                    }
                    else
                    {
                        // Return error regarding file without content
                        hasError = true;
                        message = "File contains no content";
                    }
                }
                else
                {
                    // Return error regarding null file
                    hasError = true;
                    message = "No file found";
                }
            }

            return Json(
                new
                {
                    HasError = hasError,
                    Message = message,
                    Exception = exception,
                    Document = new
                    {
                        ID = cfd.ID,
                        FILENAME = cfd.FILENAME,
                        FILEPATH = "Deprecated",
                        NAME = cfd.NAME,
                        DESCRIPTION = cfd.DESCRIPTION,
                        UPLOADDATE = cfd.UPLOADDATE.ToString()
                    }
                }
            );
        }

        #endregion Routes

        #region Private Methods

        /// <summary>
        /// Converts a file to a blob to be inserted into the db.
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        private byte[] BlobFile(string sPath)
        {
            byte[] data = null;

            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            using (FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read))
            {
                BinaryReader br = new BinaryReader(fStream);
                data = br.ReadBytes((int)numBytes);
            }

            return data;
        }

        /// <summary>
        /// Retrieves the next Xift action name from the ACTIONTRACKER table.
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        private string GetNextXiftID(int year)
        {
            int nextDigit;

            // Retrieve year
            ACTIONTRACKER actTrack = db.ACTIONTRACKERs.Where(a => a.YEAR == year).FirstOrDefault();

            // Test for year
            if (actTrack == null)
            {
                // Create record for the year
                actTrack = new ACTIONTRACKER();
                actTrack.COUNTER = 2;
                actTrack.YEAR = year;
                db.ACTIONTRACKERs.Add(actTrack);
                db.SaveChanges();
                // Initialize to first for this year
                nextDigit = 1;
            }
            else
            {
                nextDigit = actTrack.COUNTER;
                // Increment counter for given year
                actTrack.COUNTER = actTrack.COUNTER + 1;
                db.SaveChanges();
            }

            System.Text.StringBuilder actionID = new System.Text.StringBuilder();
            actionID.Append(year.ToString());
            actionID.Append(new String('0', 6 - nextDigit.ToString().Length));
            actionID.Append(nextDigit.ToString());

            return actionID.ToString();
        }

        /// <summary>
        /// Request listing based on status with paging and sorting.  Marco
        /// might want to stored procedure this mofo.
        /// </summary>
        /// <param name="byUser"></param>
        /// <returns></returns>
        private List<RequestResult> GetRequestsByStatus(bool byUser)
        {
            int days = Request["days"] == null ? 0 : int.Parse(Request["days"].ToString());
            int page = Request["page"] == null ? 1 : int.Parse(Request["page"].ToString());
            int pageSize = int.Parse(ConfigurationManager.AppSettings["pageSize"].ToString());
            int skip = (page - 1) * pageSize;
            int statusID = Request["status"] == null ? 1 : int.Parse(Request["status"].ToString());
            string sortCol = Request["sortCol"] == null ? "ActionDate" : Request["sortCol"].ToString();
            string sortDir = Request["sortDir"] == null ? "DESC" : Request["sortDir"].ToString();

            var user = byUser ? User.Identity.Name : null;

            // Build where clause
            System.Text.StringBuilder whereClause = new System.Text.StringBuilder();

            // Set status and ensure active
            if (statusID == 2)
            {
                // For the "Issued" view, we have to include both "Issued" and "Transferred"
                whereClause.Append("(FILEREQUEST.STATUSID == 2 OR FILEREQUEST.STATUSID == 6) AND (STATUSID == 2 OR STATUSID == 6)");
                whereClause.Append(" AND FILEREQUEST.ACTIVE == true AND ACTIVE == true");
            }
            else
            {
                whereClause.AppendFormat("FILEREQUEST.STATUSID == {0} AND STATUSID == {0} AND FILEREQUEST.ACTIVE == true AND ACTIVE == true", statusID);
            }

            // If showing just user requests, filter
            if (byUser) whereClause.AppendFormat(" AND FILEREQUEST.REQUESTEDBY = \"{0}\" AND REQUESTEDBY = \"{0}\"", user);

            // If date range is passed in, filter
            DateTime dateFilter = DateTime.Now.AddDays(days * -1);
            if (days > 0) whereClause.AppendFormat(" AND ActionDate >= DateTime({0}, {1}, {2})", dateFilter.Year, dateFilter.Month, dateFilter.Day);

            int total = db.FILEREQUESTAUDITs.Where(whereClause.ToString()).Count();

            int remainder;
            int pages = Math.DivRem(total, pageSize, out remainder);

            if (remainder > 0) pages++;
            ViewBag.PageCount = pages;
            ViewBag.CurrentPage = page;
            ViewBag.CurrentStatus = statusID;
            ViewBag.SortColumn = sortCol;
            ViewBag.Days = days;

            if (Request["LastSortCol"] != null && Request["LastSortCol"] == sortCol)
                sortDir = sortDir == "ASC" ? "DESC" : "ASC";

            ViewBag.SortDirection = sortDir;

            List<RequestResult> requests = db
                .FILEREQUESTAUDITs
                .Where(whereClause.ToString())
                .Select(a => new RequestResult
                {
                    ActionDate = a.ACTIONDATE,
                    AuditID = a.ID,
                    CurrentLocation = a.CASEFILE.CURRENTLOCATION,
                    FileRequestID = a.FILEREQUESTID,
                    FileRoomLocation = a.CASEFILE.FILEROOMLOCATION,
                    FullName = a.CASEFILE.FULLNAME,
                    ID = a.CASEFILE.ID,
                    RequestedBy = a.REQUESTEDBY,
                })
                .OrderBy(string.Format("{0} {1}", sortCol, sortDir))
                .Skip(skip)
                .Take(pageSize)
                .ToList<RequestResult>();

            return requests;
        }

        /// <summary>
        /// Retrieves the counts shown in the tabs in both the "All" and "My"
        /// request views.
        /// </summary>
        /// <param name="showMyRequests"></param>
        /// <returns></returns>
        private StatusCounts GetStatusCounts(bool showMyRequests)
        {
            List<FILEREQUEST> requests = null;

            if (showMyRequests)
            {
                requests = db.FILEREQUESTs.Where(r => r.REQUESTEDBY == User.Identity.Name && r.ACTIVE).ToList();
            }
            else
            {
                requests = db.FILEREQUESTs.Where(r => r.ACTIVE).ToList();
            }

            StatusCounts statusCounts = new StatusCounts
            {
                Open = requests.Where(r => r.STATUSID == 1).Count(),
                Issued = requests.Where(r => r.STATUSID == 2).Count(),
                Received = requests.Where(r => r.STATUSID == 3).Count(),
                Returned = requests.Where(r => r.STATUSID == 4).Count()
            };

            return statusCounts;
        }

        /// <summary>
        /// Sets the id digitized flag for a case file.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isDigitized"></param>
        private void UpdateIsDigitized(int id, bool isDigitized)
        {
            int count = db.CASEFILEDOCs.Where(d => d.CASEFILE.ID == id && d.ACTIVE == true).Count();

            // Determine if we need to update the CASEFILE.ISDIGITIZED field
            if (count == 0 && isDigitized || count > 0 && !isDigitized)
            {
                CASEFILE caseFile = db.CASEFILEs.Where(f => f.ID == id).FirstOrDefault();
                caseFile.ISDIGITIZED = count > 0;
                db.SaveChanges();
            }
        }

        #endregion Private Methods

        #region Public Methods

        [HttpGet]
        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        public JsonResult DeleteDocument(int id)
        {
            // Retrieve document
            CASEFILEDOC cfd = db.CASEFILEDOCs.Where(c => c.ID == id).FirstOrDefault();

            int caseFileID = cfd.CASEFILEID;

            if (cfd == null)
            {
                return Json(new { HasError = true, Message = "Document not found.", Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    // Set db record to inactive
                    cfd.ACTIVE = false;
                    db.SaveChanges();
                    // Update IsDigitized based on column count
                    UpdateIsDigitized(cfd.CASEFILE.ID, cfd.CASEFILE.ISDIGITIZED);
                    // Return success
                    return Json(new { HasError = false, Message = string.Empty, Exception = string.Empty }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { HasError = true, Message = "Server error occurred.", Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        public JsonResult DeleteFile(int id)
        {
            try
            {
                CASEFILE caseFile = db.CASEFILEs.Where(f => f.ID == id).FirstOrDefault();
                caseFile.ACTIVE = true;
                db.SaveChanges();
                return Json(new { Success = true, Message = string.Empty, Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message, Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ERMAAuthorize(Roles = "Administrator,FileRoom,SecurityAsst")]
        public JsonResult DeleteFileRequest(int id)
        {
            try
            {
                // Cancel current request
                FILEREQUEST request = db.FILEREQUESTs.Where(f => f.ID == id).FirstOrDefault();
                request.ACTIVE = false;
                // Audit the cancellation
                FILEREQUESTAUDIT fra = new FILEREQUESTAUDIT();
                fra.ACTIONDATE = DateTime.Now;
                fra.ACTIONUSER = User.Identity.Name;
                fra.ACTIVE = true;
                fra.CASEFILEID = request.CASEFILEID;
                fra.FILEREQUESTID = request.ID;
                fra.LOCATION = request.LOCATION;
                fra.REQUESTDATE = request.REQUESTDATE;
                fra.REQUESTEDBY = request.REQUESTEDBY;
                fra.REQUESTTYPEID = request.REQUESTTYPEID;
                fra.STATUSID = 7; // Canceled
                db.FILEREQUESTAUDITs.Add(fra);
                db.SaveChanges();

                // Send notification email
                ERMAEmail.CaseFileRequestCanceled(fra.FILEREQUEST);

                return Json(new { Success = true, Message = string.Empty, Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message, Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ERMAAuthorize(Roles = "Administrator,FileRoom,Scanner,SecurityAsst")]
        public JsonResult DigitizeFile(int id)
        {
            try
            {
                // Cancel current request
                FILEREQUEST request = db.FILEREQUESTs.Where(f => f.ID == id).FirstOrDefault();
                request.ACTIVE = false;
                // Audit the cancellation
                FILEREQUESTAUDIT fra = new FILEREQUESTAUDIT();
                fra.ACTIONDATE = DateTime.Now;
                fra.ACTIONUSER = User.Identity.Name;
                fra.ACTIVE = true;
                fra.CASEFILEID = request.CASEFILEID;
                fra.FILEREQUESTID = request.ID;
                fra.LOCATION = "Scan Team";
                fra.REQUESTDATE = request.REQUESTDATE;
                fra.REQUESTEDBY = request.REQUESTEDBY;
                fra.REQUESTTYPEID = request.REQUESTTYPEID;
                fra.STATUSID = 8; // Scan queue
                db.FILEREQUESTAUDITs.Add(fra);
                db.SaveChanges();

                return Json(new { Success = true, Message = string.Empty, Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message, Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ERMAAuthorize]
        public void GetFile(int id)
        {
            CASEFILEDOC doc = db.CASEFILEDOCs.Where(d => d.ID == id).FirstOrDefault();

            if (doc != null)
            {
                string fileName = Path.GetRandomFileName().Split('.')[0] + ".pdf";
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                Response.ContentType = "pdf";
                Response.BinaryWrite(doc.DOCUMENT);
                Response.End();
            }
        }

        [HttpGet]
        [ERMAAuthorize]
        public JsonResult GetFullRequestHistory(int id)
        {
            var subject = db
                .CASEFILEs.Where(c => c.ID == id)
                .AsEnumerable()
                .Select(c => new
                {
                    DOB = c.DOB.ToShortDateString(),
                    FullName = c.FULLNAME,
                    SSN = c.SSN
                })
                .FirstOrDefault();

            var audits = db
                .FILEREQUESTAUDITs
                .Where(a => a.CASEFILEID == id)
                .OrderBy(a => a.ACTIONDATE)
                .AsEnumerable()
                .Select(a => new
                {
                    ID = a.ID,
                    ActionDate = string.Format("{0} {1}", a.ACTIONDATE.ToShortDateString(), a.ACTIONDATE.ToShortTimeString()),
                    ActionUser = a.ACTIONUSER,
                    RequestDate = string.Format("{0} {1}", a.REQUESTDATE.ToShortDateString(), a.REQUESTDATE.ToShortTimeString()),
                    REQUESTEDBY = a.REQUESTEDBY,
                    RequestType = a.REQUESTTYPEID,
                    Status = a.STATUS.NAME
                })
                .ToList();

            return Json(new { Subject = subject, Audits = audits }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ERMAAuthorize]
        public JsonResult GetRequestHistory(int id)
        {
            var audits = db
                .FILEREQUESTAUDITs
                .Where(a => a.FILEREQUESTID == id)
                .OrderBy(a => a.ACTIONDATE)
                .Select(a => new
                {
                    ID = a.ID,
                    ActionDate = a.ACTIONDATE,
                    ActionUser = a.ACTIONUSER,
                    FullName = a.CASEFILE.FULLNAME,
                    DOB = a.CASEFILE.DOB,
                    SSN = a.CASEFILE.SSN,
                    RequestDate = a.REQUESTDATE,
                    RequestedBy = a.REQUESTEDBY,
                    RequestType = a.REQUESTTYPEID,
                    Status = a.STATUS.NAME
                })
                .ToList();

            return Json(audits, JsonRequestBehavior.AllowGet);
        }

        [ERMAAuthorize]
        public JsonResult SearchUsers(string query)
        {
            if (query.Trim() == string.Empty)
                return Json(string.Empty);
            else
                return Json(
                    db
                    .USERROLEs
                    .Where(u => u.USERNAME.ToUpper().Contains(query.ToUpper()))
                    .Select(u => new { UserNameEscaped = u.USERNAME.Replace("\\", "\\\\"), UserName = u.USERNAME })
                );
        }

        [HttpGet]
        [ERMAAuthorize]
        public JsonResult GetUserOptions()
        {
            try
            {
                List<string> users = db.USERROLEs.OrderBy(u => u.USERNAME).Select(u => u.USERNAME).ToList();

                System.Text.StringBuilder userOptions = new System.Text.StringBuilder();
                foreach (string user in users)
                {
                    userOptions.AppendFormat("<option value='{0}'>{0}</option>", user);
                }
                // Return success
                return Json(new { Success = true, Message = string.Empty, Users = userOptions.ToString(), Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message, Users = string.Empty, Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ERMAAuthorize]
        public JsonResult MarkAsDigitized(int requestID, int caseFileID)
        {
            try
            {
                // Retrieve current active requests for this case file
                List<FILEREQUEST> fileRequests = db
                    .FILEREQUESTs
                    .Where(r => r.CASEFILEID == caseFileID && r.ACTIVE == true && (r.STATUSID == 1 || r.STATUSID == 8))
                    .ToList();

                foreach (FILEREQUEST request in fileRequests)
                {
                    // Cancel current request
                    request.ACTIVE = false;

                    // Audit the digitization
                    FILEREQUESTAUDIT fra = new FILEREQUESTAUDIT();
                    fra.ACTIONDATE = DateTime.Now;
                    fra.ACTIONUSER = User.Identity.Name;
                    fra.ACTIVE = true;
                    fra.CASEFILEID = request.CASEFILEID;
                    fra.FILEREQUESTID = request.ID;
                    fra.LOCATION = request.LOCATION;
                    fra.REQUESTDATE = request.REQUESTDATE;
                    fra.REQUESTEDBY = request.REQUESTEDBY;
                    fra.REQUESTTYPEID = request.REQUESTTYPEID;
                    fra.STATUSID = 9; // Digitized
                    db.FILEREQUESTAUDITs.Add(fra);
                    db.SaveChanges();

                    // Send notification email to the requestor
                    ERMAEmail.CaseFileDigitized(request);
                }

                return Json(new { Success = true, Message = string.Empty, Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message, Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ERMAAuthorize]
        public JsonResult SendFile(int id, int statusID, string comments)
        {
            try
            {
                FILEREQUEST fr = db.FILEREQUESTs.Where(r => r.ID == id).FirstOrDefault();
                // Update case file request
                fr.ACTIONUSER = User.Identity.Name;
                fr.COMMENTS = comments;
                fr.LOCATION = fr.STATUS.NAME;
                fr.STATUSID = statusID;
                // Add audit record
                FILEREQUESTAUDIT audit = new FILEREQUESTAUDIT();
                audit.ACTIONUSER = fr.ACTIONUSER;
                audit.ACTIONDATE = DateTime.Now;
                audit.CASEFILEID = fr.CASEFILEID;
                audit.COMMENTS = comments;
                audit.FILEREQUESTID = fr.ID;
                audit.LOCATION = fr.STATUS.NAME;
                audit.REQUESTEDBY = fr.REQUESTEDBY;
                audit.REQUESTDATE = fr.REQUESTDATE;
                audit.REQUESTTYPEID = fr.REQUESTTYPEID;
                audit.STATUSID = fr.STATUSID;
                audit.ACTIVE = true;
                db.FILEREQUESTAUDITs.Add(audit);
                // Update case file
                CASEFILE caseFile = db.CASEFILEs.Where(c => c.ID == fr.CASEFILEID).FirstOrDefault();
                switch (fr.STATUSID)
                {
                    case 1: // Open - no need to change status
                        caseFile.CURRENTLOCATION = caseFile.FILEROOMLOCATION;
                        break;
                    case 4: // Returned
                        caseFile.CURRENTLOCATION = caseFile.FILEROOMLOCATION ?? "File room";
                        // Send notification email
                        ERMAEmail.CaseFileReceived(fr);
                        break;
                    case 5: // Deleted
                        caseFile.CURRENTLOCATION = caseFile.FILEROOMLOCATION ?? "File room";
                        break;
                    case 2: // Issued
                        caseFile.CURRENTLOCATION = "Issued to " + fr.REQUESTEDBY;
                        // Send notification email
                        ERMAEmail.CaseFileIssued(fr);
                        break;
                    case 3: // In transit
                        caseFile.CURRENTLOCATION = "In transit";
                        // Send notification email
                        ERMAEmail.CaseFileReturned(fr);
                        break;
                    case 8: // Digitize
                        caseFile.CURRENTLOCATION = "With scan team";
                        break;
                }
                // Make updates
                db.SaveChanges();
                // Return success
                return Json(new { Success = true, Message = string.Empty, Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = true, Message = ex.Message, Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ERMAAuthorize]
        public JsonResult RequestCaseFileByID(int id)
        {
            try
            {
                // Retrieve case file (if it exists)
                CASEFILE caseFile = db
                    .CASEFILEs
                    .Where(c => c.ID == id)
                    .FirstOrDefault();
                // Create request
                ERMA.FILEREQUEST fr = new ERMA.FILEREQUEST();
                fr.ACTIONUSER = User.Identity.Name;
                fr.ACTIVE = true;
                fr.CASEFILE = caseFile;
                fr.COMMENTS = string.Empty;
                fr.LOCATION = caseFile.CURRENTLOCATION ?? "FF";
                fr.REQUESTDATE = DateTime.Now;
                fr.REQUESTEDBY = User.Identity.Name;
                fr.REQUESTTYPE = db.REQUESTTYPEs.Where(t => t.NAME == "S2MS").First();
                fr.STATUS = db.STATUS.Where(r => r.NAME == "Open").First();
                db.FILEREQUESTs.Add(fr);
                // Create audit
                FILEREQUESTAUDIT fra = new FILEREQUESTAUDIT();
                fra.ACTIONUSER = fr.ACTIONUSER;
                fra.ACTIONDATE = fr.REQUESTDATE;
                fra.CASEFILE = fr.CASEFILE;
                fra.COMMENTS = fr.COMMENTS;
                fra.FILEREQUEST = fr;
                fra.LOCATION = fr.LOCATION;
                fra.REQUESTDATE = fr.REQUESTDATE;
                fra.REQUESTEDBY = fr.REQUESTEDBY;
                fra.REQUESTTYPEID = fr.REQUESTTYPEID;
                fra.STATUS = fr.STATUS;
                fra.ACTIVE = true;
                db.FILEREQUESTAUDITs.Add(fra);
                // Commit
                db.SaveChanges();
                // Send notification email
                ERMAEmail.CaseFileRequested(fr);
                // Build return object
                return Json(new { Success = true, Message = string.Empty, Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message, Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ERMAAuthorize]
        public JsonResult RequestCaseFile(string fullName, string dob, string ssn, int personID)
        {
            DateTime birthDate = DateTime.Parse(dob);
            string[] name = fullName.Replace(",", string.Empty).Split(' ');
            string last = name[0];
            string first = name[1];
            string middle = string.Empty;
            if (name.Length == 3) middle = name[2];

            try
            {
                // Retrieve case file (if it exists)
                CASEFILE caseFile = db
                    .CASEFILEs
                    .Where(c => (c.DOB == birthDate && c.FULLNAME == fullName && c.SSN == ssn) || (c.PERSONID == personID))
                    .FirstOrDefault();
                // Ensure it exists
                if (caseFile == null)
                {
                    // Create a case file record
                    caseFile = new CASEFILE();
                    caseFile.ACTIVE = true;
                    caseFile.DOB = birthDate;
                    caseFile.CURRENTLOCATION = "File room";
                    caseFile.FILEROOMLOCATION = caseFile.CURRENTLOCATION;
                    caseFile.FIRSTNAME = first;
                    caseFile.HASCLASSIFIEDHARDCOPY = false;
                    caseFile.ISDIGITIZED = false;
                    caseFile.LASTNAME = last;
                    caseFile.MIDDLENAME = middle;
                    caseFile.PERSONID = personID;
                    caseFile.SSN = ssn;
                    db.CASEFILEs.Add(caseFile);
                    db.SaveChanges();
                }
                // Process request
                return RequestCaseFileByID(caseFile.ID);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message, Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ERMAAuthorize]
        public JsonResult TransferFile(int id, int auditID, string user)
        {
            try
            {
                // Retrieve request and change requesting user
                FILEREQUEST fr = db.FILEREQUESTs.Where(r => r.ID == id).FirstOrDefault();
                fr.REQUESTEDBY = user;

                // Deactivate current "Issued" audit
                FILEREQUESTAUDIT currentAudit = db.FILEREQUESTAUDITs.Where(a => a.ID == auditID).FirstOrDefault();
                currentAudit.ACTIVE = false;

                // Create audit for transfer
                FILEREQUESTAUDIT fra = new FILEREQUESTAUDIT();
                fra.ACTIONUSER = User.Identity.Name;
                fra.ACTIONDATE = DateTime.Now;
                fra.CASEFILEID = fr.CASEFILEID;
                fra.COMMENTS = string.Format("Transferred from {0} to {1}", User.Identity.Name, user);
                fra.FILEREQUESTID = fr.ID;
                fra.LOCATION = fr.LOCATION;
                fra.REQUESTDATE = fr.REQUESTDATE;
                fra.REQUESTEDBY = fr.REQUESTEDBY;
                fra.REQUESTTYPEID = fr.REQUESTTYPEID;
                fra.STATUSID = 6; // Transferred
                fra.ACTIVE = true;
                db.FILEREQUESTAUDITs.Add(fra);

                // Update case file
                CASEFILE caseFile = db.CASEFILEs.Where(c => c.ID == fr.CASEFILEID).FirstOrDefault();
                caseFile.CURRENTLOCATION = "Transferred to " + user;

                // Make updates
                db.SaveChanges();

                // Send notification email
                ERMAEmail.CaseFileTransferred(fr);

                // Return success
                return Json(new { Success = true, Message = string.Empty, Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message, Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ERMAAuthorize]
        public JsonResult UpdateFileLocation(int fileRequestID, string location)
        {
            try
            {
                FILEREQUEST fr = db.FILEREQUESTs.Where(r => r.ID == fileRequestID).FirstOrDefault();
                fr.LOCATION = location;
                fr.CASEFILE.CURRENTLOCATION = location;
                fr.CASEFILE.FILEROOMLOCATION = location;
                db.SaveChanges();
                return Json(new { Success = true, Message = string.Empty, Exception = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.ToString(), Exception = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Public Methods

    }

    public class ListRequest
    {
        public DateTime ActionDate { get; set; }
        public DateTime DOB { get; set; }
        public string FullName { get; set; }
        public string RequestedBy { get; set; }
        public string SSN { get; set; }
    }

    public class RequestResult
    {
        public int ID { get; set; }
        public int AuditID { get; set; }
        public int FileRequestID { get; set; }
        public string FullName { get; set; }
        public string RequestedBy { get; set; }
        public DateTime ActionDate { get; set; }
        public string CurrentLocation { get; set; }
        public string FileRoomLocation { get; set; }
    }

    public class StatusCounts
    {
        public int Open { get; set; }
        public int Issued { get; set; }
        public int Received { get; set; }
        public int Returned { get; set; }
    }

}