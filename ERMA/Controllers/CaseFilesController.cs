﻿using ERMA.Code;
using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ERMA.Views.Home
{
    [ERMAAuthorize(Roles = "FileRoom,Administrator")]
    public class CaseFilesController : Controller
    {
        #region Locals

        private ERMAEntities db = new ERMAEntities();

        #endregion Locals

        #region Routes

        [HttpGet]
        [ERMAAuthorize]
        // GET: CaseFiles/Details/5
        //public async Task<ActionResult> Details(int? id)
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CASEFILE caseFile = db.CASEFILEs.Find(id);
            if (caseFile == null)
            {
                return HttpNotFound();
            }

            return View(caseFile);
        }

        [HttpGet]
        // GET: CaseFiles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CaseFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,CMSFileID,XiftID,PersonID,LastName,FirstName,MiddleName,DOB,SSN,BirthPlace,CaseFileDate,CurrentLocation,FileRoomLocation,IsDigitized,HasClassifiedHardCopy,Active")] CASEFILE caseFile)
        {
            if (ModelState.IsValid)
            {
                string actionName = GetNextXiftID(caseFile.CASEFILEDATE.Year);
                // Test if integrated with Xift
                if (bool.Parse(System.Configuration.ConfigurationManager.AppSettings["iwx"].ToString()))
                {
                    ActionCreateServiceClient client = new ActionCreateServiceClient();
                    Xift.Services.Contracts.ActionCreateInformation aci = new Xift.Services.Contracts.ActionCreateInformation();
                    aci.ActionName = actionName;
                    aci.Classification = "Top Secret"; // TODO: What classification will our case files be?
                    aci.ProgramID = 1; // ProgramID should always be 1
                    caseFile.XIFTID = client.CreateAction(aci).ToString();
                }
                // Create case file in ERMA
                db.CASEFILEs.Add(caseFile);
                db.Entry(caseFile).Property(c => c.FULLNAME).IsModified = false;
                //await db.SaveChangesAsync();
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction("Edit", "CaseFiles", new { id = caseFile.ID });
            }

            return View(caseFile);
        }

        // GET: CaseFiles/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CASEFILE caseFile = db.CASEFILEs.Find(id);
            if (caseFile == null)
            {
                return HttpNotFound();
            }
            return View(caseFile);
        }

        // POST: CaseFiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,CMSFileID,XiftID,PersonID,LastName,FirstName,MiddleName,DOB,SSN,BirthPlace,CaseFileDate,CurrentLocation,FileRoomLocation,IsDigitized,HasClassifiedHardCopy,Active")] CASEFILE caseFile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(caseFile).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                db.SaveChanges();
                return RedirectToAction("Edit", new { caseFile.ID });
                //return RedirectToAction("Subjects", "Home");
            }
            return View(caseFile);
        }

        // GET: CaseFiles/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //CASEFILE caseFile = await db.CASEFILEs.FindAsync(id);
            CASEFILE caseFile = db.CASEFILEs.Find(id);
            if (caseFile == null)
            {
                return HttpNotFound();
            }
            return View(caseFile);
        }

        // POST: CaseFiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //CASEFILE caseFile = await db.CASEFILEs.FindAsync(id);
            CASEFILE caseFile = db.CASEFILEs.Find(new object[]{ id });
            db.CASEFILEs.Remove(caseFile);
            //await db.SaveChangesAsync();
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion Routes

        #region Overrides

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion Overrides

        #region Custom Methods

        /// <summary>
        /// Retrieves the next Xift action name from the ACTIONTRACKER table.
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        private string GetNextXiftID(int year)
        {
            int nextDigit;

            // Retrieve year
            ACTIONTRACKER actTrack = db.ACTIONTRACKERs.Where(a => a.YEAR == year).FirstOrDefault();

            // Test for year
            if (actTrack == null)
            {
                // Create record for the year
                actTrack = new ACTIONTRACKER();
                actTrack.COUNTER = 2;
                actTrack.YEAR = year;
                db.ACTIONTRACKERs.Add(actTrack);
                db.SaveChanges();
                // Initialize to first for this year
                nextDigit = 1;
            }
            else
            {
                nextDigit = actTrack.COUNTER;
                // Increment counter for given year
                actTrack.COUNTER = actTrack.COUNTER + 1;
                db.SaveChanges();
            }

            System.Text.StringBuilder actionID = new System.Text.StringBuilder();
            actionID.Append(year.ToString());
            actionID.Append(new String('0', 6 - nextDigit.ToString().Length));
            actionID.Append(nextDigit.ToString());

            return actionID.ToString();
        }

        #endregion Custom Methods
    }
}
