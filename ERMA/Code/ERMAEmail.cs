﻿using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Net.Mail;

namespace ERMA.Code
{
    /// <summary>
    /// Class that drives the email templating component
    /// </summary>
    public static class ERMAEmail
    {

        #region Public Methods

        /// <summary>
        /// Email to notify user that a case file they requested has been
        /// digitized.
        /// </summary>
        /// <param name="fileRequest"></param>
        public static void CaseFileDigitized(FILEREQUEST fileRequest)
        {
            IDictionary<string, object> fields = new ExpandoObject();
            fields["DOB"] = fileRequest.CASEFILE.CASEFILEDATE.ToShortDateString();
            fields["FULLNAME"] = fileRequest.CASEFILE.FULLNAME;

            string body = GetEmailBody(fields, "digitized.tplt");

            SendEmail(fileRequest.REQUESTEDBY, "S2MS Case File Digitized", body);
        }

        /// <summary>
        /// Email to notify the user that the case file has been issued to them
        /// by the file room
        /// </summary>
        /// <param name="fileRequest"></param>
        public static void CaseFileIssued(FILEREQUEST fileRequest)
        {
            IDictionary<string, object> fields = new ExpandoObject();
            fields["DOB"] = fileRequest.CASEFILE.DOB.ToShortDateString();
            fields["FULLNAME"] = fileRequest.CASEFILE.FULLNAME;

            string body = GetEmailBody(fields, "digitized.tplt");

            SendEmail(fileRequest.REQUESTEDBY, "S2MS Case File Digitized", body);
        }

        /// <summary>
        /// Email to notify the user that the file room has received the case
        /// file they have returned.
        /// </summary>
        /// <param name="fileRequest"></param>
        public static void CaseFileReceived(FILEREQUEST fileRequest)
        {
            IDictionary<string, object> fields = new ExpandoObject();
            fields["DOB"] = fileRequest.CASEFILE.DOB.ToShortDateString();
            fields["FULLNAME"] = fileRequest.CASEFILE.FULLNAME;

            string body = GetEmailBody(fields, "received.tplt");

            SendEmail(fileRequest.REQUESTEDBY, "S2MS Case File Received", body);
        }

        /// <summary>
        /// Email to notify the user that their case file request has been
        /// received.
        /// </summary>
        /// <param name="fileRequest"></param>
        public static void CaseFileRequested(FILEREQUEST fileRequest)
        {
            IDictionary<string, object> fields = new ExpandoObject();
            fields["DOB"] = fileRequest.CASEFILE.DOB.ToShortDateString();
            fields["FULLNAME"] = fileRequest.CASEFILE.FULLNAME;

            string body = GetEmailBody(fields, "requested.tplt");

            SendEmail(fileRequest.REQUESTEDBY, "S2MS Case File Request", body);
        }

        /// <summary>
        /// Email to notify the user that their request has been canceled.
        /// </summary>
        /// <param name="fileRequest"></param>
        public static void CaseFileRequestCanceled(FILEREQUEST fileRequest)
        {
            IDictionary<string, object> fields = new ExpandoObject();
            fields["DOB"] = fileRequest.CASEFILE.DOB.ToShortDateString();
            fields["FULLNAME"] = fileRequest.CASEFILE.FULLNAME;

            string body = GetEmailBody(fields, "canceled.tplt");

            SendEmail(fileRequest.REQUESTEDBY, "S2MS Case File Request Canceled", body);
        }

        /// <summary>
        /// Email to notify the user that the file room has received the file
        /// the user previously returned.
        /// </summary>
        /// <param name="fileRequest"></param>
        public static void CaseFileReturned(FILEREQUEST fileRequest)
        {
            IDictionary<string, object> fields = new ExpandoObject();
            fields["DOB"] = fileRequest.CASEFILE.DOB.ToShortDateString();
            fields["FULLNAME"] = fileRequest.CASEFILE.FULLNAME;

            string body = GetEmailBody(fields, "returned.tplt");

            SendEmail(fileRequest.REQUESTEDBY, "S2MS Case File Returned", body);
        }

        /// <summary>
        /// Emails both the transferrer and the user that the file has been
        /// transferred to.
        /// </summary>
        /// <param name="fileRequest"></param>
        public static void CaseFileTransferred(FILEREQUEST fileRequest)
        {
            IDictionary<string, object> fields = new ExpandoObject();
            fields["ACTIONUSER"] = fileRequest.ACTIONUSER;
            fields["DOB"] = fileRequest.CASEFILE.DOB.ToShortDateString();
            fields["FULLNAME"] = fileRequest.CASEFILE.FULLNAME;
            fields["REQUESTEDBY"] = fileRequest.REQUESTEDBY;

            string body = GetEmailBody(fields, "transferred.tplt");

            SendEmail(fileRequest.ACTIONUSER, "S2MS Case File Transferred", body);

            body = GetEmailBody(fields, "transferredto.tplt");

            SendEmail(fileRequest.REQUESTEDBY, "S2MS Case File Transferred", body);
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public static void SendEmail(string userName, string subject, string body)
        {
            string to = userName.Split('\\')[1] + "@state.gov";

            MailMessage msg = new MailMessage("S2MSNotification@state.gov", to, subject, body);
            msg.BodyEncoding = System.Text.Encoding.ASCII;
            msg.IsBodyHtml = true;
            new SmtpClient().Send(msg);
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Replaces tokens in email templates with values.
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="templateFile"></param>
        /// <returns></returns>
        private static string GetEmailBody(IDictionary<string, object> fields, string templateFile)
        {
            string fileContents = string.Empty;
            string fileDirectory = System.Configuration.ConfigurationManager.AppSettings["fileRepo"].ToString();
            string path = Path.Combine(fileDirectory, "Email", templateFile);

            using (StreamReader sr = new StreamReader(path))
            {
                fileContents = sr.ReadToEnd();
            }

            foreach (KeyValuePair<string, object> kvp in fields)
            {
                // Test to see if the template contains the property
                if (fileContents.Contains(string.Format("[{0}]", kvp.Key)))
                {
                    fileContents = fileContents.Replace(string.Format("[{0}]", kvp.Key), kvp.Value.ToString().Trim());
                }
            }

            return fileContents;
        }

        #endregion Private Methods

    }
}