﻿using System.Linq;
using System.Web.Mvc;

namespace ERMA.Code
{
    /// <summary>
    /// Custom authorization attribute
    /// </summary>
    public sealed class ERMAAuthorizeAttribute : AuthorizeAttribute
    {

        #region Locals

        ERMAEntities db = new ERMAEntities();
        private readonly string[] _roles;

        #endregion Locals

        #region Constructor

        public ERMAAuthorizeAttribute(params string[] roles)
        {
            _roles = roles;
        }

        #endregion Constructor

        #region Overrides

        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            int count = 0;
            string userName = httpContext.Request.LogonUserIdentity.Name;

            if (_roles.Count() > 0)
            {
                // Search for current user and ensure they're in at least one role
                count = db
                    .USERROLEs
                    .Where(u => u.USERNAME == userName && _roles.Contains(u.ROLE.NAME))
                    .Count();
            }
            else
            {
                // Search for the current user
                count = db
                    .USERROLEs
                    .Where(u => u.USERNAME == userName)
                    .Count();
            }

            return (count > 0);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            int count = db
                .USERROLEs
                .Where(u => u.USERNAME == filterContext.HttpContext.Request.LogonUserIdentity.Name)
                .Count();

            if (count > 0)
                base.HandleUnauthorizedRequest(filterContext);
            else
                filterContext.Result = new RedirectResult("/Home/AccessDenied");
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
 	         base.OnAuthorization(filterContext);
        }

        #endregion Overrides

    }
}