﻿using System;
using System.Linq;
using System.Web.Security;

namespace ERMA.Code
{
    /// <summary>
    /// Custom role provider
    /// </summary>
    public class ERMARoleProvider : RoleProvider
    {

        #region Locals

        ERMAEntities db = new ERMAEntities();

        #endregion Locals

        #region Public Override Properties

        public override string ApplicationName
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public override string Description
        {
            get
            {
                return base.Description;
            }
        }

        public override string Name
        {
            get
            {
                return base.Name;
            }
        }

        #endregion Public Override Properties

        #region Public Override Methods

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            try
            {
                foreach (string userName in usernames)
                {
                    foreach (string roleName in roleNames)
                    {
                        USERROLE ur = new USERROLE();
                        ur.ROLEID = db.ROLEs.Where(r => r.NAME == roleName).Select(r => r.ID).FirstOrDefault();
                        ur.USERNAME = userName;
                        db.USERROLEs.Add(ur);
                    }
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void CreateRole(string roleName)
        {
            try
            {
                ROLE role = new ROLE { NAME = roleName, DESCRIPTION = string.Format("The {0} role.", roleName) };
                db.ROLEs.Add(role);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            try
            {
                ROLE role = db.ROLEs.Where(r => r.NAME == roleName).FirstOrDefault();
                db.ROLEs.Remove(role);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            try
            {
                return db.ROLEs.Select(r => r.NAME).ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string[] GetRolesForUser(string username)
        {
            try
            {
                int[] roleIDs = db.USERROLEs.Where(u => u.USERNAME == username).Select(r => r.ROLEID).ToArray();
                string[] roles = db.ROLEs.Where(r => roleIDs.Contains(r.ID)).Select(r => r.NAME).ToArray();
                return roles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            try
            {
                int roleID = db.ROLEs.Where(r => r.NAME == roleName).Select(r => r.ID).FirstOrDefault();
                return db.USERROLEs.Where(u => u.ROLEID == roleID).Select(u => u.USERNAME).ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            try
            {
                return (db.USERROLEs.Where(u => u.USERNAME == username && u.ROLE.NAME == roleName).Count() > 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            try
            {
                foreach (string userName in usernames)
                {
                    foreach (string roleName in roleNames)
                    {
                        USERROLE ur = db.USERROLEs.Where(u => u.USERNAME == userName && u.ROLE.NAME == roleName).FirstOrDefault();
                        db.USERROLEs.Remove(ur);
                    }
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override bool RoleExists(string roleName)
        {
            try
            {
                return db.ROLEs.Where(r => r.NAME == roleName).Count() > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        #endregion Public Override Methods

        #region Custom Methods

        /// <summary>
        /// Determines if user is in roles passed in.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="roles"></param>
        /// <returns></returns>
        public bool IsUserInRole(string username, string[] roles)
        {
            try
            {
                return (db.USERROLEs.Where(u => u.USERNAME == username && roles.Contains(u.ROLE.NAME)).Count() > 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Custom Methods

    }
}