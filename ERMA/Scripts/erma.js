﻿// Code to fix bug with IE where ajax call isn't made unless debugger is running
$(document).ready(function () {
    $.ajaxSetup({ cache: false })
});

function fnHideModal(modalName) {
    $("#" + modalName).modal("hide");
}

function fnShowModal(modalName) {
    $("#" + modalName).modal("show");
}

function fnFormatDate(theDate, includeTime) {

    var parsedDate = new Date(new Date(parseInt(theDate.substr(6))));
    var jsDate = new Date(parsedDate);

    if (includeTime) {
        var minutes = parsedDate.getMinutes();
        if (minutes < 10) minutes = "0" + minutes;
        return (parsedDate.getMonth() + 1) + "/" + parsedDate.getDate() + "/" + parsedDate.getUTCFullYear() + " " +
            parsedDate.getHours() + ":" + minutes;
    }
    else {
        return (parsedDate.getMonth() + 1) + "/" + parsedDate.getDate() + "/" + parsedDate.getUTCFullYear();
    }
}

function fnErrorMessage(errMsg, detailedErr) {
    $("#errorMessage").text(errMsg);
    $("#stackTrace").val(detailedErr);
    fnShowModal("errorDialog")
}
